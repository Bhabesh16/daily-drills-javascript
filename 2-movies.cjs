const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
};


/*
    NOTE: For all questions, the returned data must contain all the movie information including its name.

    Q1. Find all the movies with total earnings more than $500M. 
    Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
    Q.3 Find all movies of the actor "Leonardo Dicaprio".
    Q.4 Sort movies (based on IMDB rating)
        if IMDB ratings are same, compare totalEarning as the secondary metric.
    Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
        drama > sci-fi > adventure > thriller > crime

    NOTE: Do not change the name of this file
*/

// Question 1

function getMoviesMoreThan500M() {

    const allMoviesMoreThan500M = Object.entries(favouritesMovies)
        .filter((movie) => {
            const totalEarnings = movie[1].totalEarnings.replace('$', '')
                .replace('M', '');

            return Number(totalEarnings) > 500;
        });

    return Object.fromEntries(allMoviesMoreThan500M);
}

// Question 2

function getMoviesMin3OscarsMoreThan500M() {

    const allMoviesMin3OscarsMoreThan500M = Object.entries(favouritesMovies)
        .filter((movie) => {

            const totalEarnings = movie[1].totalEarnings.replace('$', '')
                .replace('M', '');

            return movie[1].oscarNominations > 3 && Number(totalEarnings) > 500;

        });

    return Object.fromEntries(allMoviesMin3OscarsMoreThan500M);
}

// Question 3

function getMoviesFromActorName() {

    const actorName = 'Leonardo Dicaprio';

    const allMoviesFromActorName = Object.entries(favouritesMovies)
        .filter((movie) => {

            const getActor = movie[1].actors
                .find((actor) => {
                    return actor.includes(actorName);
                });

            return getActor !== undefined;
        });

    return Object.fromEntries(allMoviesFromActorName);
}

// Question 4

function getSortedMoviesInIMDB() {

    const allSortedMoviesInIMDB = Object.entries(favouritesMovies)
        .sort((movie1, movie2) => {

            function getTotalEarnings(earnings) {

                const totalEarnings = earnings.replace('$', '')
                    .replace('M', '');

                return Number(totalEarnings);
            }

            const totalEarningsMovie1 = getTotalEarnings(movie1[1].totalEarnings);
            const totalEarningsMovie2 = getTotalEarnings(movie2[1].totalEarnings);

            return (movie2[1].imdbRating - movie1[1].imdbRating) || (totalEarningsMovie2 - totalEarningsMovie1);
        });

    return Object.fromEntries(allSortedMoviesInIMDB);
}

// Question 5

function getMoviesGroupOnGenre() {

    const moviePriorityList = ['crime', 'thriller', 'adventure', 'sci-fi', 'drama'];

    const allMoviesGroupOnGenre = Object.entries(favouritesMovies)
        .reduce((allMoviesGroup, movie) => {

            const priorityNumber = movie[1].genre
                .reduce((highestPriority, genre) => {

                    const priority = moviePriorityList.reduce((getIndex, genreFromList, index) => {

                        if (genre.includes(genreFromList)) {
                            getIndex = Math.max(getIndex, index);
                        }

                        return getIndex;
                    }, null);

                    highestPriority = Math.max(highestPriority, priority);
                    return highestPriority;

                }, null);

            if (allMoviesGroup[moviePriorityList[priorityNumber]] === undefined) {
                allMoviesGroup[moviePriorityList[priorityNumber]] = [];
            }
            allMoviesGroup[moviePriorityList[priorityNumber]].push(movie);

            return allMoviesGroup;
        }, {});

    return allMoviesGroupOnGenre;

}

// Q1. Find all the movies with total earnings more than $500M.

const allMoviesMoreThan500M = getMoviesMoreThan500M();
console.log(allMoviesMoreThan500M);

// Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.

const allMoviesMin3OscarsMoreThan500M = getMoviesMin3OscarsMoreThan500M();
console.log(allMoviesMin3OscarsMoreThan500M);

// Q.3 Find all movies of the actor "Leonardo Dicaprio".

const allMoviesFromActorName = getMoviesFromActorName();
console.log(allMoviesFromActorName);

// Q.4 Sort movies (based on IMDB rating)
//         if IMDB ratings are same, compare totalEarning as the secondary metric.

const allSortedMoviesInIMDB = getSortedMoviesInIMDB();
console.log(allSortedMoviesInIMDB);

// Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
//         drama > sci-fi > adventure > thriller > crime

const allMoviesGroupOnGenre = getMoviesGroupOnGenre();
console.log(allMoviesGroupOnGenre);